import 'package:flutter/material.dart';
import 'package:flutter_database/models/employee_model.dart';
import 'package:flutter_database/providers/employe_provider.dart';
import 'package:provider/provider.dart';

class EmployeePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Consumer<EmployeeProvider>(
          builder: (_, EmployeeProvider employeeProvider, __){
            // print('999999999999999999999999999999999999999');
            // print(employeeProvider);
            // print('999999999999999999999999999999999999999');
            if(employeeProvider.emplyeeModel.employes!.isEmpty)
              return Text("No Data");
            return SingleChildScrollView(
              child: Column(
                children: [

                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(), //ปิดสกอร์ของ list builder
                    shrinkWrap: true, // เปลี่ยนเป็น true
                    itemBuilder: (_ , int index){
                      Employe employee = employeeProvider.emplyeeModel.employes![index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 0,horizontal: 10),
                        child: Card(
                          elevation: 3,
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Name : ${employee.employeeName}"),
                                Text("Salary : ${employee.employeeSalary} DH"),
                                Text("Age : ${employee.employeeAge} years"),

                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    itemCount: employeeProvider.emplyeeModel.employes!.length,
                  ),
                ],
              ),
            );
          })
    );
  }
}

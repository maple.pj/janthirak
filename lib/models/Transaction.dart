class Transaction{
  String? title; //ชื่อรายการ
  double? amount; // จำนวนเงิน
  DateTime? date; // วันที่ แวลา บันทึกรายการ

Transaction({this.title,this.amount,this.date});
}
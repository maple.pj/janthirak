import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_database/screens/employee_page.dart';
import 'package:flutter_database/services/repository.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Repository repository = Repository();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_callApi);
  }

  _gotToNextPage(){
    Navigator.of(context).pushReplacement(CupertinoPageRoute(
      builder: (_) => EmployeePage(),
    ));
  }

  _callApi(_){
    repository.fetchEmployes(context);
    _gotToNextPage();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.cyanAccent,
    );
  }
}


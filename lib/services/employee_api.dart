import 'package:flutter_database/models/employee_model.dart';
import 'package:http/http.dart';

class EmployeeApi{
  Client _client = Client();

  Future<EmplyeeModel> fetchEmployees()async{
    Response response = await _client.get(Uri.parse("https://dummy.restapiexample.com/api/v1/employees")
    );
    if(response.statusCode == 200){
      print('response.statusCode: ${response.statusCode}');
      print(response.body);
      return emplyeeModelFromJson(response.body);
    }
    return EmplyeeModel(employes: []);

  }

}
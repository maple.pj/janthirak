// To parse this JSON data, do
//
//     final emplyeeModel = emplyeeModelFromJson(jsonString);

import 'dart:convert';

EmplyeeModel emplyeeModelFromJson(String str) => EmplyeeModel.fromJson(json.decode(str));

String emplyeeModelToJson(EmplyeeModel data) => json.encode(data.toJson());

class EmplyeeModel {
  EmplyeeModel({
    this.status,
    this.employes,
  });

  String? status;
  List<Employe>? employes;

  factory EmplyeeModel.fromJson(Map<String, dynamic> json) => EmplyeeModel(
    status: json["status"] == null ? null : json["status"],
    employes: json["data"] == null ? null : List<Employe>.from(json["data"].map((x) => Employe.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": employes == null ? null : List<dynamic>.from(employes!.map((x) => x.toJson())),
  };
}

class Employe {
  Employe({
    this.id,
    this.employeeName,
    this.employeeSalary,
    this.employeeAge,
    this.profileImage,
  });

  int? id;
  String? employeeName;
  int? employeeSalary;
  int? employeeAge;
  String? profileImage;

  factory Employe.fromJson(Map<String, dynamic> json) => Employe(
    id: json["id"] == null ? null : json["id"],
    employeeName: json["employee_name"] == null ? null : json["employee_name"],
    employeeSalary: json["employee_salary"] == null ? null : json["employee_salary"],
    employeeAge: json["employee_age"] == null ? null : json["employee_age"],
    profileImage: json["profile_image"] == null ? null : json["profile_image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "employee_name": employeeName == null ? null : employeeName,
    "employee_salary": employeeSalary == null ? null : employeeSalary,
    "employee_age": employeeAge == null ? null : employeeAge,
    "profile_image": profileImage == null ? null : profileImage,
  };
}

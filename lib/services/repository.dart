import 'package:flutter/cupertino.dart';
import 'package:flutter_database/providers/employe_provider.dart';
import 'package:provider/provider.dart';

class Repository {

 Future<void> fetchEmployes(BuildContext context) async {
   EmployeeProvider employeeProvider = Provider.of(context, listen: false);
   await employeeProvider.fetchEmployes();
 }

}
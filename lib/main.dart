import 'package:flutter/material.dart';
import 'package:flutter_database/models/Transaction.dart';
import 'package:flutter_database/providers/employe_provider.dart';
import 'package:flutter_database/providers/transaction_provider.dart';
import 'package:flutter_database/screens/employee_page.dart';
import 'package:flutter_database/screens/form_screen.dart';
import 'package:flutter_database/screens/splash_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      //นิยามและรอรับค่า
      providers: [
        ChangeNotifierProvider(create: (_) => EmployeeProvider()),
        ChangeNotifierProvider(create: (context) => TransactionProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        // title: 'Flutter Demo',
        // theme: ThemeData(
        //   primarySwatch: Colors.blue,
        // ),
        home: SplashScreen(),
      )
    );
  }
}

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key, required this.title}) : super(key: key);
//
//   final String title;
//
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;
//
//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//         actions: [
//           IconButton(
//               icon: Icon(Icons.add),
//               onPressed: (){
//                 Navigator.push(context, MaterialPageRoute(builder: (context) =>form_screen())
//                 );
//             },
//           )
//         ],
//       ),
//       bottomNavigationBar: Row(
//         children: [
//           Expanded(
//             child: FlatButton(
//               child: Text("Next Employee_page"),
//               color: Colors.purple,
//               textColor: Colors.white,
//               onPressed: (){
//                 Navigator.push(context, MaterialPageRoute(builder: (context) =>EmployeePage())
//                 );
//               },
//             ),
//           ),
//         ],
//       ),
//       body: Consumer(builder: (context, TransactionProvider provider, child) {
//         var count = provider.transactions.length; // นับจำนวนข้อมูล
//         if(count<=0){
//           return Center(
//             child: Text("ไม่พบข้อมูล",style: TextStyle(fontSize: 20,color: Colors.red),)
//           );
//         } else{
//           return ListView.builder(
//               itemCount: count, //มีProvider เท่าไหร่ให้แสดงเท่านั้น
//               itemBuilder: (context, int index) {
//                 Transaction data = provider.transactions[index]; //ดึงรายการแต่ละรายการ จาก list ที่อยู่ใน provider มาทีละแถว
//                 return Card(
//                   elevation: 3,
//                   margin: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
//                   child: ListTile(
//                     leading: CircleAvatar(
//                       radius: 30,
//                       child: FittedBox(
//                         child: Text(data.amount.toString()),
//                       ),
//                     ),
//                     title: Text(data.title.toString()),
//                     subtitle: Text(data.date.toString()),
//                   ),
//                 );
//               });
//         }
//       }
//
//         )
//     );
//   }
// }

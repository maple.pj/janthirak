import 'package:flutter/material.dart';
import 'package:flutter_database/models/Transaction.dart';
import 'package:flutter_database/providers/transaction_provider.dart';
import 'package:flutter_database/screens/form_screen.dart';
import 'package:provider/provider.dart';

class ShowDataPage extends StatefulWidget {
  @override
  _ShowDataPageState createState() => _ShowDataPageState();
}

class _ShowDataPageState extends State<ShowDataPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Show Data"),
        actions: [
          IconButton(
              icon: Icon(Icons.add),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>form_screen())
                );
            },
          )
        ],
      ),
      body: Consumer(builder: (context, TransactionProvider provider, child) {
        var count = provider.transactions.length; // นับจำนวนข้อมูล
        if(count<=0){
          return Center(
            child: Text("ไม่พบข้อมูล",style: TextStyle(fontSize: 20,color: Colors.red),)
          );
        } else{
          return ListView.builder(
              itemCount: count, //มีProvider เท่าไหร่ให้แสดงเท่านั้น
              itemBuilder: (context, int index) {
                Transaction data = provider.transactions[index]; //ดึงรายการแต่ละรายการ จาก list ที่อยู่ใน provider มาทีละแถว
                return Card(
                  elevation: 3,
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                  child: ListTile(
                    leading: CircleAvatar(
                      radius: 30,
                      child: FittedBox(
                        child: Text(data.amount.toString()),
                      ),
                    ),
                    title: Text(data.title.toString()),
                    subtitle: Text(data.date.toString()),
                  ),
                );
              });
        }
      }

        )
    );
  }
}

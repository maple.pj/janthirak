import 'package:flutter/cupertino.dart';
import 'package:flutter_database/models/Transaction.dart';

class TransactionProvider with ChangeNotifier{
  //ตัวอย่างข้อมูล
  List<Transaction> transactions = [];

  //ดึงข้อมูล
    List<Transaction> getTransaction(){
    return transactions;
  }
  void addTransaction(Transaction statement){
      transactions.insert(0,statement); // ใช้ insert เมื่อให้ข้อมูลอยุ่ด้านบน ใช้ add เพื่อเพิ่มข้อมูลจะอยู่ด้านล่างสุด
      notifyListeners(); // แจ้งเตือนไปที่ consumer ว่ามีการเปลี่ยนข้แมูลแล้ว
  }
}
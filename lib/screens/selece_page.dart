import 'package:flutter/material.dart';
import 'package:flutter_database/screens/employee_page.dart';

class SelecePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(left: 20,right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(child:  ElevatedButton(
                  style:  ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),primary: Colors.orangeAccent,fixedSize: Size(0, 50)),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> EmployeePage()));
                  },
                  child: const Text('Employees'),
                ),)
              ],
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Expanded(child: ElevatedButton(
                  style:  ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),primary: Colors.cyan,fixedSize: Size(0, 50)),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> EmployeePage()));
                  },
                  child: const Text('Form Save Data'),
                ),)
              ],
            )
          ],
        ),
      ),
    );
  }
}

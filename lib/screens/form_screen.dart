import 'package:flutter/material.dart';
import 'package:flutter_database/models/Transaction.dart';
import 'package:flutter_database/providers/transaction_provider.dart';
import 'package:provider/provider.dart';

class form_screen extends StatefulWidget {
  @override
  _form_screenState createState() => _form_screenState();
}

class _form_screenState extends State<form_screen> {
  final formKey = GlobalKey<FormState>();
  // controller

  final titleController = TextEditingController(); // รับค่าชื่อรายการ
  final amountController = TextEditingController(); // รับตัวเลขจำนวนเงิน
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("แบบฟอร์มบันทึกข้อมูล"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: titleController,
                    decoration: InputDecoration(
                        labelText: "ชื่อรายการ"
                    ),
                    autofocus: true,
                    validator: (str){
                      //กรรีชื่อรายการเป็นค่าว่าง
                      if(str!.isEmpty){
                        return "กรุณาป้อนชื่อรายการ";
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: amountController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "จำนวนเงิน"
                    ),
                    validator: (str){
                      if(str!.isEmpty){
                        return "กรุณาป้อนจำนวนเงิน";
                      }

                      if(double.parse(str)<=0) {
                        return "กรุณาป้อนตัวเลขมากกว่า 0";
                      }
                      return null;
                    },
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: FlatButton(
                          child: Text("เพิ่มข้อมูล"),
                          color: Colors.purple,
                          textColor: Colors.white,
                          onPressed: (){
                            if(formKey.currentState!.validate()){
                              var title = titleController.text; // สร้างตัวแปลมารับ เพื่อแสดงข้อมูลที่ผู้ใช้ป้อน
                              var amount = amountController.text;

                              // เตรียมข้อมูลลง provider
                              Transaction statement = Transaction(
                                title: title,
                                amount: double.parse(amount),
                                date: DateTime.now()

                              ); // object

                              // เรียก provider
                              var provider = Provider.of<TransactionProvider>(context,listen: false);
                              provider.addTransaction(statement);
                              Navigator.pop(context);
                            }
                          },
                        ),
                      ),
                    ],
                  )
                ],
              )
          ),
        )
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

